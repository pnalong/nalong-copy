package org.api.service;

import org.api.model.Message;
import org.api.utility.BaseDataResponse;

public interface MessageService {
	BaseDataResponse getMessageAll();
	BaseDataResponse getMessage(Integer id);
	BaseDataResponse insert(Message message);
	BaseDataResponse update(Message message);
	BaseDataResponse delete(Message message);
}
