package org.api.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.api.dao.MessageDao;
import org.api.model.Message;
import org.api.service.MessageService;
import org.api.utility.BaseDataResponse;
import org.api.utility.BaseSysStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class MessageServiceImpl implements MessageService{
	
	@Autowired
	private MessageDao dao;
	
	@Override
	public BaseDataResponse getMessageAll() {
		BaseDataResponse response = new BaseDataResponse();
		try {
			List<?> messages = dao.getMessageAll();
			response.setData(messages);
			response.setStatus(messages == null || messages.size()==0 ? BaseSysStatus.NOT_FOUND.value() : BaseSysStatus.FOUND.value());
			response.setStatusCode(HttpStatus.OK.value());
			response.setMessage("");
			
			return response;
		}catch (SQLException e) {
			e.printStackTrace();
			
			response.setStatus(BaseSysStatus.FAILD.value());
			response.setStatusCode(HttpStatus.OK.value());
			response.setData(null);
			response.setMessage("");
		}
		return response;
	}

	@Override
	public BaseDataResponse getMessage(Integer id) {
		BaseDataResponse response = new BaseDataResponse();
		try {
			Object message = dao.getMessage(id);
			response.setData(message);
			response.setStatus(message == null ? BaseSysStatus.NOT_FOUND.value() : BaseSysStatus.FOUND.value());
			response.setStatusCode(HttpStatus.OK.value());
			response.setMessage("");
			
			return response;
		}catch (SQLException e) {
			e.printStackTrace();
			
			response.setStatus(BaseSysStatus.FAILD.value());
			response.setStatusCode(HttpStatus.OK.value());
			response.setData(null);
			response.setMessage("");
		}
		return response;
	}

	@Override
	public BaseDataResponse insert(Message message) {
		BaseDataResponse response = new BaseDataResponse();
		try {
			Object obj = dao.getMessage(1);
			response.setData(obj);
			response.setStatus(BaseSysStatus.INSERT.value());
			response.setStatusCode(HttpStatus.OK.value());
			response.setMessage("");
			return response;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public BaseDataResponse update(Message message) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BaseDataResponse delete(Message message) {
		// TODO Auto-generated method stub
		return null;
	}

}
