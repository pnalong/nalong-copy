package org.api.utility;

public class BaseDataApplication<T> {
	
	private T data;
	
	public void add(T data) {
		this.data = data;
	}

	public T get() {
		return data;
	}
}
