package org.api.utility;

import java.util.stream.Collectors;

import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Component
@Data
@AllArgsConstructor
@NoArgsConstructor
@Scope("prototype")
public class BaseDataResponse {
	private String status;
	private String message;
	private int statusCode;
	private Object data;
	
	public BaseDataResponse(BindingResult result) {
		this.status = HttpStatus.BAD_REQUEST.name();
		this.message = HttpStatus.BAD_REQUEST.name();
		this.statusCode = HttpStatus.BAD_REQUEST.value();
		this.data = result.getFieldErrors().stream().collect(Collectors.toMap(o -> o.getField(), o -> o.getDefaultMessage()));
	}
	
}
