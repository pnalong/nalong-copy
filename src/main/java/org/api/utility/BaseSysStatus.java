package org.api.utility;

public enum BaseSysStatus {
	INSERT("INSERTED"),
	UPDATE("UPDATED"),
	DELETE("DELETED"),
	FAILD("FAILED"),
	LINKED("LINKED"),
	FOREIGN_KEY_CONSTRAIN("FOREIGN_KEY_CONSTRAIN"),
	EXIST("EXIST"),
	NOT_EXIST("NOT_EXIST"),
	SUCCESS("SUCCESS"),
	FOUND("FOUND"),
	NOT_FOUND("NOT_FOUND"),
	INVALIDED("INVALIDED")
	;
	
	private String value;
	
	private BaseSysStatus(String value) {
		this.value = value;
	}
	
	public String value() {
		return this.value;
	}
}
