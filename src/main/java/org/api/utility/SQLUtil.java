package org.api.utility;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SQLUtil {

	public static String bindParameter(String sql, Map<String, String> param){
		for (Map.Entry<String, String> entry : param.entrySet()) {
			sql.replace("{"+entry.getKey()+"}", entry.getValue());
		}
		return sql;
	}
	
	public static List<Map<String, Object>> aliasToMap(ResultSet rs){
		try {
			ArrayList<Map<String, Object>> arr = new ArrayList<>();
			ResultSetMetaData meta = (ResultSetMetaData) rs.getMetaData();
			Map<String, Object> map = null; 
			while(rs.next()){
				 map = new HashMap<>();
				 for (int i = 1; i <= meta.getColumnCount(); i++) {
	                map.put(meta.getColumnLabel(i), rs.getObject(i));
	            }
				arr.add(map);
			}
			return arr;		
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public static Map<String, Object> aliasToSingleMap(ResultSet rs){
		try {
			ResultSetMetaData meta = (ResultSetMetaData) rs.getMetaData();
			Map<String, Object> map = null; 
			while(rs.next()){
				 map = new HashMap<>();
				 for (int i = 1; i <= meta.getColumnCount(); i++) {
					 map.put(meta.getColumnLabel(i), rs.getObject(i));
	            }
			}
			rs.close();
			return map;			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
}
