package org.api.model;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(description = "All details about the Message.")
public class Message {
	
	@ApiModelProperty(notes = "The id of the message")
	private Integer id;
	
	@ApiModelProperty(notes = "The text of the message", required=true)
	@NotNull(message="The text cannot empty or null")
	@Size(min=2, message="The text should have atleast 2 characters")
	@Size(max=300, message="The text should have less than 300 characters")
	private String text;
	
	@ApiModelProperty(notes = "The code of the message", required=true)
	@NotNull(message="The code cannot empty or null")
	@DecimalMin(message="The code must be greater than 0", value = "1")
	@DecimalMax(message="The code must be less than 999", value="999")
	private int code;
	
	@ApiModelProperty(notes = "The description of the message", required=false)
	private String description;
	
	public Message(String text) {
		this.text = text;
	}
	public Message(Integer id) {
		this.id = id;
	}
	
}
