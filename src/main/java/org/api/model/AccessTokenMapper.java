package org.api.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.ToString;

@ToString
public class AccessTokenMapper {

	private String id;
	private List<String> authorities = new ArrayList<String>();
	private String first_name;
	private String last_name;
	private String user_type;
	private String mobile;
	private String country;
	private String username;
	
	

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getUser_type() {
		return user_type;
	}

	public void setUser_type(String user_type) {
		this.user_type = user_type;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
	
	@SuppressWarnings("unchecked")
	public AccessTokenMapper(Object detail) {
		System.out.println();
		if(detail != null && !detail.equals("")) {
			Map<String, Object> map =(Map<String, Object>) detail;
			this.authorities = (List<String>) map.get("authorities");
			this.first_name = map.get("first_name").toString();
			this.country = map.get("country").toString();
			this.mobile = map.get("mobile").toString();
			this.last_name = map.get("last_name").toString();
			this.user_type = map.get("user_type").toString();
			this.id = map.get("id").toString(); 
			this.username = map.get("user_name").toString(); 
		}
		
	}

	public AccessTokenMapper() {
		
	}
	
}
