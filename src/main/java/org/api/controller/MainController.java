package org.api.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {
	
	@RequestMapping({ "/" })
	public String index(ModelMap model, HttpSession session, HttpServletRequest request) {
	
		return "index";
	}
	
	@RequestMapping({ "/login" })
	public String login(ModelMap model, HttpSession session, HttpServletRequest request) {
	
		return "login";
	}
	
	
}
