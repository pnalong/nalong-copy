package org.api.configuration;

import static springfox.documentation.schema.AlternateTypeRules.newRule;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.async.DeferredResult;
import com.fasterxml.classmate.TypeResolver;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.WildcardType;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.Contact;
import springfox.documentation.service.GrantType;
import springfox.documentation.service.OAuth;
import springfox.documentation.service.ResourceOwnerPasswordCredentialsGrant;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.DocExpansion;
import springfox.documentation.swagger.web.ModelRendering;
import springfox.documentation.swagger.web.OperationsSorter;
import springfox.documentation.swagger.web.SecurityConfiguration;
import springfox.documentation.swagger.web.SecurityConfigurationBuilder;
import springfox.documentation.swagger.web.TagsSorter;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger.web.UiConfigurationBuilder;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	@Value("${config.oauth2.tokenUri}")
	private String accessTokenUri;
    public static final String securitySchemaOAuth2 = "oauth2schema";
    public static final String authorizationScopeGlobal = "global";
    public static final String authorizationScopeGlobalDesc ="accessEverything";
	
	@Autowired
	private TypeResolver typeResolver;

	@Bean
	UiConfiguration uiConfig() {
		return UiConfigurationBuilder.builder()
		        .deepLinking(true)
		        .displayOperationId(true)
		        .defaultModelsExpandDepth(1)
		        .defaultModelExpandDepth(1)
		        .defaultModelRendering(ModelRendering.EXAMPLE)
		        .displayRequestDuration(true)
		        .docExpansion(DocExpansion.NONE)
		        .filter(true)
		        .maxDisplayedTags(null)
		        .operationsSorter(OperationsSorter.ALPHA)
		        .showExtensions(true)
		        .tagsSorter(TagsSorter.ALPHA)
		        .supportedSubmitMethods(UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS)
		        .validatorUrl(null)
		        .build();
	}
	
	@Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.basePackage("org.api"))
		        .paths(PathSelectors.ant("/api/**"))
		        .build()
		        .pathMapping("/").directModelSubstitute(LocalDate.class, String.class)
				.genericModelSubstitutes(ResponseEntity.class)
				.alternateTypeRules(newRule(
						typeResolver.resolve(DeferredResult.class,typeResolver.resolve(ResponseEntity.class, WildcardType.class)),
						typeResolver.resolve(WildcardType.class)))
                .securityContexts(Collections.singletonList(securityContext()))
                .securitySchemes(Arrays.asList(securitySchema()))
                .apiInfo(apiInfo());
    }

    @Bean
    public SecurityScheme apiKey() {
        return new ApiKey(HttpHeaders.AUTHORIZATION, "apiKey", "header");
    }

    @Bean
    public SecurityScheme apiCookieKey() {
        return new ApiKey(HttpHeaders.COOKIE, "apiKey", "cookie");
    }

    private OAuth securitySchema() {

        List<AuthorizationScope> authorizationScopeList = new ArrayList<AuthorizationScope>();
        authorizationScopeList.add(new AuthorizationScope("read", "for read operations"));
        authorizationScopeList.add(new AuthorizationScope("write", "for write operations"));
        authorizationScopeList.add(new AuthorizationScope("trust", "for trust operations"));

        List<GrantType> grantTypes = new ArrayList<GrantType>();
        GrantType passwordCredentialsGrant = new ResourceOwnerPasswordCredentialsGrant(accessTokenUri);
        grantTypes.add(passwordCredentialsGrant);

        return new OAuth("oauth2", authorizationScopeList, grantTypes);
    }

    private SecurityContext securityContext() {
        return SecurityContext
        		.builder()
        		.securityReferences(defaultAuth())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        final AuthorizationScope[] authorizationScopes = { 
        		new AuthorizationScope("read",  "for read operations"), 
        		new AuthorizationScope("write", "for write operations"), 
        		new AuthorizationScope("trust", "for trust operations") 
        };
        return Collections.singletonList(new SecurityReference("oauth2", authorizationScopes));
    }  
	
	@Bean
	SecurityConfiguration security() {
	    return SecurityConfigurationBuilder.builder()
	        .clientId("sereyvong")
	        .clientSecret("123123")
	        .realm("sereyvong")
	        .appName("Bearer access token  sereyvong98@gmail.com")
	        .scopeSeparator(" ")
	        .additionalQueryStringParams(null)
	        .useBasicAuthenticationWithAccessCodeGrant(false)
	        .build();
	  }
	
	
	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("API")
				.description("")
                .termsOfServiceUrl("https://www.sereyvong.me/api")
                .contact(new Contact("Developers", "www.sereyvong.me/api", "mailto:sereyvong98@gmail.com"))
                .license("Open Source")
                .licenseUrl("\"https://www.apache.org/licenses/LICENSE-2.0")
                .version("1.0.0")
                .build();
	}
		
}
