package org.api.rest;

import org.api.model.Message;
import org.api.service.MessageService;
import org.api.utility.BaseDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.ws.rs.Produces;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.Authorization;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/messages")
//@PreAuthorize("hasRole('ROLE_API') and #oauth2.hasScope('write')")
//@ApiIgnore
@Api(description = "For message", produces = "application/json", tags = {"Messages"})
public class MessageRestController {
	
	@Autowired
	private MessageService messageService;
	
	@GetMapping
	@Produces({"application/json", "application/json;concept=events;v=1"})
	@ApiOperation(
	        value = "", 
	        response = BaseDataResponse.class,
	        notes = " ",
	        authorizations = {
	                @Authorization(value = "oauth2", scopes = {}),
	                @Authorization(value = "oauth2-cc", scopes = {}),
	                @Authorization(value = "oauth2-ac", scopes = {}),
	                @Authorization(value = "oauth2-rop", scopes = {}),
	                @Authorization(value = "Bearer")
	        },
	        produces = "application/json, application/json;concept=events;v=1",
	        nickname = "List all message"
    )
	@ApiResponses(value = {@ApiResponse(code = 415, message = "Content type not supported.")})
	public BaseDataResponse findAllMessage(){
		return messageService.getMessageAll();
	}
	
	@GetMapping("/{id}")
	@PreAuthorize("#oauth2.hasScope('trust')")
	@Produces({"application/json", "application/json;concept=events;v=1"})
	@ApiOperation(
	        value = "", 
	        response = BaseDataResponse.class,
	        notes = "",
	        authorizations = {
	                @Authorization(value = "oauth2", scopes = {}),
	                @Authorization(value = "oauth2-cc", scopes = {}),
	                @Authorization(value = "oauth2-ac", scopes = {}),
	                @Authorization(value = "oauth2-rop", scopes = {}),
	                @Authorization(value = "Bearer")
	        },
	        produces = "application/json,  application/json;concept=events;v=1",
	        nickname = "List message by message id"
    )
	@ApiResponses(value = {@ApiResponse(code = 415, message = "Content type not supported.")})
	public BaseDataResponse findMessage(
		@PathVariable("id")
		Integer id
	){
		return messageService.getMessage(id);
	}
	

	@PostMapping
	@PreAuthorize("#oauth2.hasScope('write')")
	@Produces({"application/json", "application/json;concept=events;v=1"})
	@ApiOperation(
	        value = "", 
	        response = BaseDataResponse.class,
	        notes = "",
	        authorizations = {
	                @Authorization(value = "oauth2", scopes = {}),
	                @Authorization(value = "oauth2-cc", scopes = {}),
	                @Authorization(value = "oauth2-ac", scopes = {}),
	                @Authorization(value = "oauth2-rop", scopes = {}),
	                @Authorization(value = "Bearer")
	        },
	        produces = "application/json,  application/json;concept=events;v=1",
	        nickname = "insert new message"
    )
	@ApiResponses(value = {@ApiResponse(code = 415, message = "Content type not supported.")})
	public BaseDataResponse saveMessage(@Validated @RequestBody Message message, BindingResult result) {
		if(result.hasErrors()) {
			return new BaseDataResponse(result);	
		}
		return messageService.insert(message);
	}
	
	@PreAuthorize("#oauth2.hasScope('trust')")
	@DeleteMapping("/{id}")
	@Produces({"application/json", "application/json;concept=events;v=1"})
	@ApiOperation(
	        value = "delete message by id", 
	        response = BaseDataResponse.class,
	        notes = "the record delete permanent",
	        authorizations = {
	                @Authorization(value = "oauth2", scopes = {}),
	                @Authorization(value = "oauth2-cc", scopes = {}),
	                @Authorization(value = "oauth2-ac", scopes = {}),
	                @Authorization(value = "oauth2-rop", scopes = {}),
	                @Authorization(value = "Bearer")
	        },
	        produces = "application/json,  application/json;concept=events;v=1",
	        nickname = "delete message by id"
    )
	@ApiResponses(value = {@ApiResponse(code = 415, message = "Content type not supported.")})
	@ApiIgnore("")
	public BaseDataResponse removeMessage(@PathVariable("id") Integer id) {
		return messageService.delete(new Message(id));
	}
	
}
