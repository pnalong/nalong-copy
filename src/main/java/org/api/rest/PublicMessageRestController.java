package org.api.rest;

import org.api.service.MessageService;
import org.api.utility.BaseDataResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import io.swagger.annotations.Api;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/api/public/messages")
@Api(description = "this api for message public", produces = "application/json", tags = {"Public Messages"})
@ApiIgnore
public class PublicMessageRestController {
	@Autowired
	private MessageService messageService;
	
	@GetMapping
	public BaseDataResponse findAllMessage(){
		return messageService.getMessageAll();
	}
	
	@GetMapping("/{id}")
	public BaseDataResponse findMessage(@PathVariable Integer id) {
		return messageService.getMessage(id);
	}
}
