package org.api.rest;

import org.api.dao.PermissionResourceDAO;
import org.api.model.AccessTokenMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/permissions")
@Api(description = "For user permission", produces = "application/json", tags = {"Administration"})
public class PermissionResource {

	@Autowired
	PermissionResourceDAO permissionResourceDAO;
	
	@PreAuthorize("hasAnyRole('view_permission', 'SUPERADMIN')")
	@GetMapping
	public ResponseEntity<Object> getListOfPermissions() {
		AccessTokenMapper accessTokenMapper = new AccessTokenMapper(((OAuth2AuthenticationDetails) SecurityContextHolder.getContext().getAuthentication().getDetails()).getDecodedDetails());
		System.out.println(accessTokenMapper.toString());
		return new ResponseEntity<Object>(permissionResourceDAO.getListOfPermissions(), HttpStatus.OK);
	}
}
