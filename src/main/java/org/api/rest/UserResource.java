package org.api.rest;

import org.api.dao.UserResourceDAO;
import org.api.model.UserModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import io.swagger.annotations.Api;

@RestController
@RequestMapping("/api/users")
@Api(description = "For user permission", produces = "application/json", tags = {"Administration"})
public class UserResource {

	@Autowired
	UserResourceDAO userResourceDAO;

	@PreAuthorize("hasAnyRole('view_users', 'SUPERADMIN')")
	@GetMapping
	public ResponseEntity<Object> getListOfUsers() {
		return new ResponseEntity<>(userResourceDAO.getListOfUsers(), HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('delete_users', 'SUPERADMIN')")
	@DeleteMapping("{id}")
	public ResponseEntity<Object> deleteUser(@PathVariable("id") String user_id) {
		
		userResourceDAO.deleteUser(user_id);
		return new ResponseEntity<>("User deleted successfully", HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('edit_users', 'SUPERADMIN')")
	@PutMapping("{id}")
	public ResponseEntity<Object> updateUser(@PathVariable("id") String user_id, @RequestBody UserModel userModel) {
		userResourceDAO.updateUser(user_id, userModel);
		return new ResponseEntity<>("User updated successfully", HttpStatus.OK);
	}

	@PreAuthorize("hasAnyRole('create_users', 'SUPERADMIN')")
	@PostMapping("/insert")
	public ResponseEntity<Object> createUser(@RequestBody UserModel userModel) {
		userResourceDAO.createUser(userModel);
		return new ResponseEntity<>("User created successfully", HttpStatus.OK);
	}
}
