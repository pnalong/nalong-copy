package org.api.dao;

import java.sql.SQLException;
import java.util.List;

import org.api.model.Message;


public interface MessageDao {
	List<?> getMessageAll() throws SQLException;
	Object getMessage(Integer id) throws SQLException;
	boolean insert(Message message) throws SQLException;
	boolean update(Message message) throws SQLException;
	boolean delete(Message message) throws SQLException;
}
