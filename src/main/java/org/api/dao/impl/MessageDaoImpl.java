package org.api.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.api.dao.MessageDao;
import org.api.model.Message;
import org.api.utility.SQLUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;



@Repository
public class MessageDaoImpl implements MessageDao{
	
	@Autowired
	private DataSource dataSource;

	@Override
	public List<?> getMessageAll() throws SQLException {
		String sql = "SELECT id, text FROM message ORDER BY id;";
		try(
			Connection con = dataSource.getConnection(); 
			PreparedStatement ps = con.prepareStatement(sql)
		){
			return SQLUtil.aliasToMap(ps.executeQuery());
		}
	}

	@Override
	public Object getMessage(Integer id) throws SQLException {
		String sql = "SELECT id, text FROM message where id = ?;";
		try(
			Connection con = dataSource.getConnection(); 
			PreparedStatement ps = con.prepareStatement(sql)
		){
			ps.setInt(1, id);
			return SQLUtil.aliasToSingleMap(ps.executeQuery());
		}
	}

	@Override
	public boolean insert(Message message) throws SQLException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean update(Message message) throws SQLException {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean delete(Message message) throws SQLException {
		// TODO Auto-generated method stub
		return true;
	}
	
	
}
