/**
 *	PROJECT			:		SGE
 *	DEVELOPER		:		COOLBEANS
 *	DEVELOPER URI	:		https://coolbeans.studio/
 *	DATE			:		06-June-2019
 */
 
$( function() {

	var SGE = {
		
		init: function() {

            this.loadBackgroundImage();
            this.smoothPageScroll();
            this.materialForm();
			
		},

        loadBackgroundImage: function() {

            if ( $( '.has-bg' ).length ) {

                $( '.has-bg' ).each( function() {

                    var bg = $( this ).attr( 'data-bg' );

                    $( this ).css({

                        'background' : 'url( ' + bg + ' ) no-repeat center center',

                        'background-size' : 'cover'

                    });

                });

            }

        },

        smoothPageScroll: function() {

            var _that = this;

            var navbarHeight = $( '.navbar' ).height();

            $( 'a[href*="#"]:not([href="#"])' ).not( '.watch-video' ).click( function() {

                if ( location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname ) {

                    var target = $( this.hash );

                    target = target.length ? target : $( '[name=' + this.hash.slice(1) +']' );

                    if ( target.length ) {

                        $( 'html, body' ).animate({

                            /* Make sure the part of the section is not hidden behind the fixed navbar */

                            scrollTop: target.offset().top - navbarHeight

                        }, 500 );

                        return false;

                    }

                }
            });

        },

        materialForm: function() {
            $( '.material-form .field-label' ).on( 'click', function( e ) {

                e.preventDefault();

                $( this ).addClass( 'active' );
                $( this ).parents( '.form-group' ).find( '.form-control' ).focus();

            });

            $( '.material-form .form-control' ).on( 'focus', function( e ) {

                e.preventDefault();

                $( this ).parents( '.form-group' ).find( '.field-label' ).addClass( 'active' );

            });

            $( '.material-form .form-control' ).on( 'blur', function() {

                $( '.material-form .field-label' ).removeClass( 'active' );

            });
        }
		
	};
	
	$( document ).ready( function() {

        SGE.init();
		
	});
	
});